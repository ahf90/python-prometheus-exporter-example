import os
import time
from prometheus_client import start_http_server, Counter

EXPORTER_PORT = os.getenv('EXPORTER_PORT', 8000)
SECONDS_ALIVE = Counter('seconds_alive', '# of seconds the container has been running')


def add_to_seconds_alive():
    SECONDS_ALIVE.inc()


if __name__ == '__main__':
    start_http_server(int(EXPORTER_PORT))
    while True:
        time.sleep(1)
        add_to_seconds_alive()
